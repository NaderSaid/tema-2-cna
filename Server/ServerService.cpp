#include "ServerService.h"
#include <fstream>


::grpc::Status ServerService::ZodiacSign(::grpc::ServerContext* context, const::Date* date, ::Sign* sign)
{
    static std::array<MonthSigns, 12> zodiacMonths;
    int separatorDay; std::string firstSign, secondSign;
    std::ifstream zodiacFile("signsText.txt");

    //     for (int i=0;i<12;i++)
    for (MonthSigns& month: zodiacMonths)
       {
           zodiacFile >> separatorDay;
           zodiacFile >> firstSign;
           zodiacFile >> secondSign;
           month.setSign(separatorDay, firstSign, secondSign);
       }

    std::string monthString = date->date().substr(0, 2);
    int month = std::stoi(monthString);
    std::string dayString = date->date().substr(3, 4);
    int day = std::stoi(dayString);

    if (day <= zodiacMonths[month - 1].getseparatorDay()) 
    {
        sign->set_sign(zodiacMonths[month - 1].getFirstSign());
    }
    else
    { 
        sign->set_sign(zodiacMonths[month - 1].getSecondSign()); 
    }
    return ::grpc::Status::OK;
}