#pragma once
#include <iostream>
#include <string>

class MonthSigns
{
public:

    friend std::istream& operator >> (std::istream& is, MonthSigns& month);

    void setSign(int& separatorDay, const std::string& firstSign, const std::string& secondSign);

    int getseparatorDay();
    std::string getFirstSign();
    std::string getSecondSign();
private:
    int separatorDay;
    std::string firstSign;
    std::string secondSign;
};