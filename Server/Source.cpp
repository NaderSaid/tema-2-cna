#include <iostream>
#include <fstream>
#include <array>
#include "MonthSigns.h"
#include <ServerReply.grpc.pb.h>
#include "ServerService.h"

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>

//static std::array<MonthSigns, 12> zodiacMonths;

int main()
{
    std::string server_address("localhost:8888");
    ServerService service;

    ::grpc_impl::ServerBuilder serverBuilder;
    serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    serverBuilder.RegisterService(&service);
    std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());

    std::cout << "Server listenig on: " << server_address << std::endl;
    server->Wait();
}