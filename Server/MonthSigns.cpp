#include "MonthSigns.h"

std::istream& operator>>(std::istream& is, MonthSigns& month)
{
	is >> month.separatorDay;
	is >> month.firstSign;
	is >> month.secondSign;
	return is;
}

void MonthSigns::setSign(int& separatorDay, const std::string& firstSign, const std::string& secondSign)
{
	this->separatorDay = separatorDay;
	this->firstSign = firstSign;
	this->secondSign = secondSign;
}

int MonthSigns::getseparatorDay()
{
	return separatorDay;
}

std::string MonthSigns::getFirstSign()
{
	return firstSign;
}

std::string MonthSigns::getSecondSign()
{
	return secondSign;
}