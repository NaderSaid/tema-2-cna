#pragma once
#include <ServerReply.grpc.pb.h>
#include "MonthSigns.h"
#include <array>

class ServerService final : public Zodiac::Service
{
public:
	ServerService() {};
	::grpc::Status ZodiacSign(::grpc::ServerContext* context, const ::Date* date, ::Sign* sign) override;
	
};