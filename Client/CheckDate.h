#pragma once
#include <iostream>
#include <string>

class CheckDate
{
public:
	friend std::ostream& operator << (std::ostream& os, const CheckDate& date);
	friend std::istream& operator >> (std::istream& is, std::string& date);

	bool validateDate(const std::string& date);
private:
	CheckDate changeToDate(const std::string& dateInput);
	bool isDate(const std::string& date);
	bool isLeapYear(const CheckDate& date);
	int day;
	int month;
	int year;
	//date must be this model 02/12/2010 -> month/day/year
};