#include "CheckDate.h"

std::ostream& operator<<(std::ostream& os, const CheckDate& date)
{
	os << date.month << "/" << date.day << "/" << date.year;
	return os;
}

std::istream& operator>>(std::istream& is, std::string& date)
{
	is >> date;
	return is;
}

bool CheckDate::isDate(const std::string& date)
{
	for (char index : date)
		if (!isdigit(index) && index != '/')
			return false;
	return true;
}

bool CheckDate::isLeapYear(const CheckDate& date)
{
	if (date.year % 4 != 0)
		return false;
	if (date.year % 100 != 0)
		return true;
	if (date.year % 400 != 0)
		return false;
	return true;
}

CheckDate CheckDate::changeToDate(const std::string& inputDate) 
{
	std::string month, day, year;
	month = inputDate.substr(0, 2);
	day = inputDate.substr(3, 4);
	year = inputDate.substr(6, 9);

	CheckDate date;
	date.month = std::stoi(month);
	date.day = std::stoi(day);
	date.year = std::stoi(year);

	return date;
}

bool CheckDate::validateDate(const std::string& dateInput)
{
	if (!isDate(dateInput))
	{
		std::cout << "Invalid input! " << std::endl;
		return false;
	}

	CheckDate date = changeToDate(dateInput);
	if ((date.year < 1000) || (date.year > 9999))
	{
		std::cout << "Year not available!" << std::endl;
		return false;
	}
	if ((date.month < 1) || (date.month > 12))
	{
		std::cout << "Month does not exist!" << std::endl;
		return false;
	}
	if ((date.day < 1) || (date.day > 31))
	{
		std::cout << "Day does not exist!" << std::endl;
		return false;
	}
	if ((date.month == 4 || date.month == 6 || date.month == 9 || date.month == 11) && date.day > 30)
	{
		std::cout << "This month only has 30 days!" << std::endl;
		return false;
	}
	if (isLeapYear(date) && date.month == 2 && date.day > 29)
	{
		std::cout << "February has 29 days this year!" << std::endl;
		return false;
	}
	if (!isLeapYear(date) && date.month == 2 && date.day > 28)
	{
		std::cout << "February only has 28 days this year!" << std::endl;
		return false;
	}
	return true;
}