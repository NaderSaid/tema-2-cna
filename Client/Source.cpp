#include <iostream>
#include <ServerReply.grpc.pb.h>
#include <Sign.grpc.pb.h>
#include <Date.grpc.pb.h>
#include "CheckDate.h"

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
    grpc_init();
    ClientContext context;
    CheckDate CheckDate;
    Date date;
    Sign sign;
    std::string dateInput;

    std::cout << "Input your birthdate: ";
    std::cin >> dateInput;

    auto stub = Zodiac::NewStub(grpc::CreateChannel("localhost:8888",
        grpc::InsecureChannelCredentials()));

    if (CheckDate.validateDate(dateInput))
    {
        date.set_date(dateInput);
        auto status = stub->ZodiacSign(&context, date, &sign);
        std::cout << "Your sign is " << sign.sign() << std::endl;
    }

    return 0;
}